---
title: Home
body_classes: title-center title-h1h2

---

###Let’swork.to/gether website launches to help support creatives affected by the Covid-19 crisis

Created as a positive response to support creatives around the world, Let’s Work Together aims to offer businesses unprecedented access to an impressive pool of talent.

Tuesday, 12 May 2020 — After witnessing the devastating financial impact of COVID-19 on creative agencies and freelancers, a small group of designers and developers have responded with Let’s Work Together, a website that promotes talent to businesses that need to adapt quickly.

“In addition to the tragic health crisis, my industry clearly suffered a financial blow. I saw creatives on my Twitter feed announcing they had just been laid off or had the projects they were working on simply cancelled.” says Stephen Voisey, Let’s Work Together’s co-founder.

“At the same time, we recognized that many businesses would need creative and digital solutions as the world increasingly shifted online,” adds Andy Cooke, the website’s co-founder and creative director. “As the lockdowns continue, the idea is to reach out to creatives affected and match them with businesses looking for fast solutions.”

####Specialities to help businesses survive and thrive
Let’s Work Together’s roster already boasts a wide range of freelancers and agencies with distinct specialties. From Website Designers through to Marketing and SEO experts, the site offers exactly the sort of talent businesses should be taking advantage of.

####100% free forever
The website is simple to use and costs nothing to join. Businesses can find talent that is ready to work, with the knowledge that they are helping to support those whose livelihoods have been directly affected by the global pandemic.

####Creatives and businesses alike can find the website at:

[https://letswork.to/gether](https://letswork.to/gether)
